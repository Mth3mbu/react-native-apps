import { Platform } from 'react-native';
import Colors from '../constants/Colors';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import NewPlaces from '../sccreens/NewPlaces';
import Map from '../sccreens/Map';
import PlaceDetails from '../sccreens/PlaceDetails';
import PlacesList from '../sccreens/PlacesList';

const PlacesNavigator = createStackNavigator(
  {
    Places: PlacesList,
    Details: PlaceDetails,
    NewPlace: NewPlaces,
    Map: Map,
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : '',
      },
      headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary,
    },
  }
);

export default createAppContainer(PlacesNavigator);
