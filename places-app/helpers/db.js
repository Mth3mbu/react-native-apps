import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('places.db');

export const init = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'CREATE TABLE IF NOT EXISTS Places (id INTEGER PRIMARY KEY NOT NULL, title TEXT NOT NULL, imageUri TEXT NOT NULL, address TEXT NOT NULL, lat REAL NOT NULL, lon REAL NOT NULL)',
        [],
        () => {
          resolve();
        },
        (_, error) => {
          reject(error);
        }
      );
    });
  });

  return promise;
};

export const insertPlace = (title, imageUri, address, lat, lon) => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'INSERT INTO Places (title, imageUri, address, lat, lon) VALUES (?,?,?,?,?)',
        [title, imageUri, address, lat, lon],
        (_, results) => {
          resolve(results);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });

  return promise;
};

export const fetchtPlaces = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'SELECT * FROM Places',
        [],
        (_, results) => {
          resolve(results);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });

  return promise;
};
