import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Button } from 'react-native';
import * as Notifications from 'expo-notifications';

Notifications.setNotificationHandler({
  handleNotification: async () => {
    return {
      shouldPlaySound: true,
      shouldShowAlert: true,
    };
  },
});

export default function App() {
  const [pushNotificationToken, setToken] = useState();

  const foregroundNotificationHamdler = () => {
    const subscription = Notifications.addNotificationReceivedListener(
      (notification) => {
        console.log(notification);
      }
    );

    return () => {
      subscription.remove();
    };
  };

  const backgroundNotificationHandler = () => {
    const subscription = Notifications.addNotificationResponseReceivedListener(
      (response) => {
        console.log(response);
      }
    );

    return () => {
      subscription.remove();
    };
  };

  const signAppToDiliverPushNotifications = async () => {
    const respomse = await Notifications.getExpoPushTokenAsync();
    const token = respomse.data;
    if (token) {
      setToken(token);
    }
  };

  const sendPushNotification = async () => {
    console.log(pushNotificationToken);
    try {
      fetch('https://api.expo.dev/v2/push/send', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Accept-Encoding': 'gzip,deflate',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          to: pushNotificationToken,
          data: { device: 'Iphone 12 Mini' },
          title: 'Black Friday Deals',
          body: "Don't miss out on our awesome black Friday deals on car suspension, reasonable prices",
        }),
      });
    } catch (e) {
      console.log(e);
    }
  };

  const sendLocalNotification = async () => {
    await Notifications.scheduleNotificationAsync({
      content: {
        title: 'Local Notification',
        body: 'The first local notification i am sending',
      },
      trigger: {
        seconds: 10,
      },
    });
  };

  useEffect(() => {
    const verifyPermissions = async () => {
      const permissions = await Notifications.requestPermissionsAsync();
      if (permissions.status !== 'granted') {
        return;
      }
      signAppToDiliverPushNotifications();
    };

    verifyPermissions();
    foregroundNotificationHamdler();
    backgroundNotificationHandler();
  }, []);

  const triggerNotificationHandler = async () => {
    await sendLocalNotification();
    await sendPushNotification();
  };

  return (
    <View style={styles.container}>
      <Button
        title='Trigger Notification'
        onPress={triggerNotificationHandler}
      />
      <StatusBar style='auto' />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
