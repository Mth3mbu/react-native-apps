import * as ProductsActions from '../actions/products';

const initialState = { products: [], total: 0, error: null };

export default (state = initialState, action) => {
  switch (action.type) {
    case ProductsActions.FETCH_PRODUCTS:
      var existingItem = state.products.some(
        (product) => product.id === action.products[0].id
      );

      if (existingItem) {
        return state;
      }
      return { ...state, products: state.products.concat(action.products) };

    case ProductsActions.GET_PRODUCTS_TOTAL:
      return { ...state, total: action.total };

    case ProductsActions.FETCH_PRODUCTS_ERROR:
      return { ...state, error: action.error };

    default:
      return state;
  }
};
