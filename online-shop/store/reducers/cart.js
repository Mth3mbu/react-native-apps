import {
  ADD_ITEM,
  REMOVE_ITEM,
  DELETE_ITEM,
  RESET_CART,
} from '../actions/cart';

const initialState = { cartItems: [] };
const getProductById = (id, array) => array.find((item) => item.id === id);
const getStateCopy = (array) => array.map((item) => item);

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET_CART:
      return { ...state, cartItems: [] };
    case ADD_ITEM:
      const cartProducts = getStateCopy(state.cartItems);
      const existingItem = getProductById(action.itemId, cartProducts);

      if (existingItem) {
        existingItem.totalItems++;
        cartProducts.map((item) =>
          item.id === action.itemId ? existingItem : item
        );
      } else {
        const item = { id: action.itemId, totalItems: 1 };
        cartProducts.push(item);
      }
      return { ...state, cartItems: cartProducts };

    case REMOVE_ITEM: {
      let cartItems = getStateCopy(state.cartItems);
      const cartItem = getProductById(action.itemId, cartItems);
      if (cartItem.totalItems === 1) {
        cartItems = cartItems.filter((item) => item.id !== action.itemId);
      } else {
        cartItem.totalItems--;
      }
      return { ...state, cartItems: cartItems };
    }

    case DELETE_ITEM: {
      let cartItems = getStateCopy(state.cartItems);
      cartItems = cartItems.filter((item) => item.id !== action.itemId);

      return { ...state, cartItems: cartItems };
    }

    default:
      return state;
  }
};
