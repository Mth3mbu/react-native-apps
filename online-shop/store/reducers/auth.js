import * as AuthActions from '../actions/auth';

const initialState = { jwtToken: null, error: null, isLogedIn: false };

export default (state = initialState, action) => {
  switch (action.type) {
    case AuthActions.LOGIN:
      return { ...state, jwtToken: action.token, isLogedIn: true };

    case AuthActions.LOGIN_ERROR:
      return { ...state, error: action.errorMessage };

    default:
      return state;
  }
};
