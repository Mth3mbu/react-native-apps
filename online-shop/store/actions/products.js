export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const GET_PRODUCTS_TOTAL = 'GET_PRODUCTS_TOTAL';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';

import { ApiUrl } from '../../constants/Enviroment';

export const fetchProducts = (jwtToken, pageOffset) => {
  return async (dispatch) => {
    try {
      const response = await fetch(
        `${ApiUrl}/products?offset=${pageOffset}&&rows=10`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      const products = await response.json();
      dispatch({ type: FETCH_PRODUCTS, products: products });
    } catch (ex) {
      dispatch({ type: FETCH_PRODUCTS_ERROR, error: ex.message });
    }
  };
};

export const getProductsTotal = (jwtToken) => {
  return async (dispatch) => {
    try {
      const response = await fetch(`${ApiUrl}/products/total`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
      });

      const productsTotal = await response.json();
      dispatch({ type: GET_PRODUCTS_TOTAL, total: productsTotal });
    } catch (ex) {
      dispatch({ type: FETCH_PRODUCTS_ERROR, error: ex.message });
    }
  };
};
