import { ApiUrl } from '../../constants/Enviroment';
export const LOGIN = 'LOGIN';
export const LOGIN_ERROR = 'LOGIN_ERROR';
import { insertToken } from '../../helpers/db';

export const StartLogin = (username, password, navigation) => {
  return async (dispatch) => {
    try {
      const user = JSON.stringify({ email: username, password: password });
      const response = await fetch(`${ApiUrl}/auth/login`, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
          'Content-Type': 'application/json',
        },
        body: user,
      });

      const resData = await response.json();
      await insertToken(resData.token);
      dispatch({ type: LOGIN, token: resData.token});
      navigation.navigate('Home',{screen: 'Products'});
    } catch (ex) {
      console.log(ex);
      dispatch({ type: LOGIN_ERROR, errorMessage: ex.message });
    }
  };
};
