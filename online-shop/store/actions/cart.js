export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const DELETE_ITEM = 'DELETE_ITEM';
export const RESET_CART = 'RESET_CART';

export const AddToCart = (itemId) => {
  return (dispatch) => {
    dispatch({ type: ADD_ITEM, itemId });
  };
};

export const RemoveItemFromCart = (itemId) => {
  return (dispatch) => {
    dispatch({ type: REMOVE_ITEM, itemId });
  };
};

export const ResetCart = () => {
  return (dispatch) => {
    dispatch({ type: RESET_CART });
  };
};

export const DeleteItemFromCart = (itemId) => {
  return (dispatch) => {
    dispatch({ type: DELETE_ITEM, itemId });
  };
};
