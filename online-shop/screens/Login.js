import React, { useReducer, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button,
  ActivityIndicator,
} from 'react-native';
import * as Colors from '../constants/Colors';
import Input from '../components/ui/Input';
import { useDispatch } from 'react-redux';
import * as AuthActions from '../store/actions/auth';

const FORM_INPUT_UPDATE = 'UPDATE';
const INPUT_BLUR = 'INPUT_BLUR';

const loginFormReducer = (state, action) => {
  switch (action.type) {
    case FORM_INPUT_UPDATE: {
      const updatedValues = {
        ...state.inputValues,
        [action.input]: action.value,
      };
      const updatedValidities = {
        ...state.inputValidities,
        [action.input]: action.isValid,
      };

      let isFormValid = true;
      for (const key in updatedValidities) {
        isFormValid = isFormValid && updatedValidities[key];
      }

      return {
        ...state,
        formIsValid: isFormValid,
        inputValues: updatedValues,
        inputValidities: updatedValidities,
      };
    }
    case INPUT_BLUR:
      const updateTouched = {
        ...state.inputTouched,
        [action.input]: true,
      };
      return {
        ...state,
        inputTouched: updateTouched,
      };

    default:
      return state;
  }
};

const LoginScreen = (props) => {
  const [isLoading, setLoading] = useState(false);
  const registerHandler = () => {
    props.navigation.navigate('Register');
  };

  const [formState, dispatchFormState] = useReducer(loginFormReducer, {
    inputValues: {
      username: '',
      password: '',
    },
    inputValidities: {
      username: false,
      password: false,
    },
    inputTouched: {
      username: false,
      password: false,
    },
    formIsValid: false,
  });

  const dispatch = useDispatch();

  const loginHandler = async () => {
    if (formState.formIsValid) {
      setLoading(true);
      await dispatch(
        AuthActions.StartLogin(
          formState.inputValues.username,
          formState.inputValues.password,
          props.navigation
        )
      );
    }
  };

  const lostFocusHandler = (inputIdentifier) => {
    dispatchFormState({ type: INPUT_BLUR, input: inputIdentifier });
  };

  const textChangedHandler = (inputIdentifier, text) => {
    let isValid = false;
    if (inputIdentifier === 'username') {
      isValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text);
    } else {
      if (text.trim().length > 6) {
        isValid = true;
      }
    }

    dispatchFormState({
      type: FORM_INPUT_UPDATE,
      value: text,
      isValid: isValid,
      input: inputIdentifier,
    });
  };

  const recoverPasswordHandler = () => {
    props.navigation.navigate('RecoverPassword');
  };
  return (
    <View style={styles.container}>
      {isLoading && <ActivityIndicator size='large' color={Colors.Primary} />}
      <Input
        placeholder='Email'
        value={formState.inputValues.username}
        isTouched={formState.inputTouched.username}
        errorMessage='Please enter a valid Email!'
        isValid={formState.inputValidities.username}
        onTextChanged={textChangedHandler.bind(this, 'username')}
        returnKeyType='next'
        lostFocus={lostFocusHandler.bind(this, 'username')}
      />

      <Input
        secureTextEntry={true}
        placeholder='Password'
        isTouched={formState.inputTouched.password}
        value={formState.inputValues.password}
        errorMessage='Password must be atleast longer than 6 Charecters'
        isValid={formState.inputValidities.password}
        onTextChanged={textChangedHandler.bind(this, 'password')}
        lostFocus={lostFocusHandler.bind(this, 'password')}
      />

      <View>
        <Button title='Login' color={Colors.Primary} onPress={loginHandler} />
      </View>

      <View style={styles.linksContainer}>
        <Text style={styles.link} placeholder onPress={recoverPasswordHandler}>
          Forgot Password?
        </Text>
        <Text style={styles.link} onPress={registerHandler}>
          Sign Up
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 15,
  },

  linksContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 15,
  },
  link: {
    textDecorationLine: 'underline',
    color: Colors.Primary,
  },
});

export default LoginScreen;

export const loginScreenOptions = (navData) => {
  return {
    headerShown: true,
    headerTintColor: Colors.White,
    headerLeft: () => null,
    headerRight: () => null,
  };
};
