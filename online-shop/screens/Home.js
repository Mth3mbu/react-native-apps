import React from 'react';
import { View, Text, Platform, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/ui/HeaderButtons';
import Products from '../components/ui/products/ProductsListing';
import * as Colors from '../constants/Colors';

const Home = (props) => {
  return (
    <View style={styles.container}>
      <Products navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  cartIconContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  cartCounterContainer: {
    position: 'absolute',
    backgroundColor: Colors.Accent,
    width: 16,
    height: 16,
    borderRadius: 15 / 2,
    right: 10,
    top: +10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  cartCounter: {
    alignItems: 'center',
    justifyContent: 'center',
    color: '#FFFFFF',
    fontSize: 8,
  },
});

export const screenOptions = (navData) => {
  const cartItems = useSelector((state) => state.cart.cartItems);
  let totalItems =
    cartItems.length > 0
      ? cartItems
          .map((item) => item.totalItems)
          ?.reduce((prev, cur) => prev + cur, 0)
      : 0;
  return {
    headerStyle: {
      backgroundColor: Colors.Primary,
    },
    headerTintColor: Colors.White,
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title='Menu'
          iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu-outline'}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
    headerRight: () => (
      <View style={styles.cartIconContainer}>
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
            title='Cart'
            iconName={
              Platform.OS === 'android' ? 'md-cart' : 'ios-cart-outline'
            }
            onPress={() => {
              navData.navigation.navigate('Cart');
            }}
          />
        </HeaderButtons>
        <View style={styles.cartCounterContainer}>
          <Text style={styles.cartCounter}>{totalItems}</Text>
        </View>
      </View>
    ),
  };
};

export default Home;
