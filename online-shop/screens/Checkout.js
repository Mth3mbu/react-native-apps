import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ActivityIndicator,
} from 'react-native';
import CreditCard from 'react-native-credit-card-v2';
import Input from '../components/ui/Input';
import * as Colors from '../constants/Colors';
import { getCardToken } from '../helpers/stripe';
import { pay } from '../helpers/payment';
import { useSelector, useDispatch } from 'react-redux';
import { createOrder } from '../helpers/order';
import { ResetCart } from '../store/actions/cart';

const Checkout = (props) => {
  const authToken = useSelector((state) => state.auth.jwtToken);
  const cartItems = useSelector((state) => state.cart.cartItems);
  const products = useSelector((state) => state.products.products);
  const [isLoading, setLoading] = useState(false);
  const dispatch = useDispatch();

  let totalPrice =
    cartItems.length > 0
      ? cartItems
          .map((item) => {
            const product = products.find((product) => product.id === item.id);
            return product.price * item.totalItems;
          })
          ?.reduce((prev, cur) => prev + cur, 0)
      : 0;

  const fields = {
    name: 1,
    cardNo: 2,
    expiryMonth: 3,
    expiryYear: 4,
    cvc: 5,
  };

  const [card, setCard] = useState({
    type: 'mastercard',
    cvc: null,
    name: '',
    number: null,
    expiry: '',
    focused: null,
  });

  const payCard = {
    'card[number]': card.number,
    'card[exp_month]': card.expiry.toString().substring(0, 2),
    'card[exp_year]': card.expiry.toString().substring(2),
    'card[cvc]': card.cvc,
  };

  const checkoutHandler = async () => {
    try {
      setLoading(true);
      const cardToken = await getCardToken(payCard);
      const response = await pay(
        {
          email: 'bongmusam@hotmail.com',
          description: 'Order 001',
          sourceToken: cardToken,
          amount: totalPrice,
        },
        authToken
      );
      if (response.isPaid) {
        await createOrder(cartItems, authToken);
        dispatch(ResetCart());
      }
      setLoading(false);
    } catch (ex) {
      console.log(ex);
      setLoading(false);
    }
  };

  const onTextChangedHandler = (feildType, value) => {
    switch (feildType) {
      case fields.name: {
        const cardState = { ...card, name: value };
        return setCard(cardState);
      }
      case fields.cardNo: {
        const cardState = { ...card, number: value };
        return setCard(cardState);
      }
      case fields.expiryMonth: {
        const cardState = { ...card, expiry: value };
        return setCard(cardState);
      }
      case fields.expiryYear: {
        const val = `${card.expiry}${value}`;
        if (val.length < 4) {
          return;
        }
        const cardState = { ...card, expiry: val };
        return setCard(cardState);
      }
      case fields.cvc:
        const cardState = { ...card, cvc: value };
        return setCard(cardState);

      default:
        return card;
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <CreditCard
          type={card.type}
          shiny={false}
          bar={true}
          focused={card.focused}
          number={card.number}
          name={card.name}
          expiry={card.expiry}
          cvc={card.cvc}
        />
      </View>

      <View style={styles.cardInput}>
        <Input
          returnKeyType='next'
          placeholder='Account Full Name'
          onTextChanged={onTextChangedHandler.bind(this, 1)}
        />
        <Input
          returnKeyType='next'
          placeholder='Card Number'
          keyboardType='numeric'
          maxLength={16}
          minLength={16}
          onTextChanged={onTextChangedHandler.bind(this, 2)}
        />
        <View style={styles.cardidentifier}>
          <View style={styles.flex}>
            <Input
              keyboardType='numeric'
              maxLength={2}
              maxLength={2}
              placeholder='Month'
              onTextChanged={onTextChangedHandler.bind(this, 3)}
            />
          </View>
          <View style={styles.flex}>
            <Input
              returnKeyType='next'
              keyboardType='numeric'
              maxLength={2}
              maxLength={2}
              placeholder='Year'
              onTextChanged={onTextChangedHandler.bind(this, 4)}
            />
          </View>
          <Input
            returnKeyType='next'
            placeholder='CVC'
            maxLength={3}
            maxLength={3}
            keyboardType='numeric'
            onTextChanged={onTextChangedHandler.bind(this, 5)}
          />
        </View>
      </View>
      <View>
        {isLoading && <ActivityIndicator size='small' color={Colors.Primary} />}
      </View>
      <View style={styles.payBtnContainer}>
        <TouchableOpacity style={styles.payBtn} onPress={checkoutHandler}>
          <Text style={{ color: Colors.White }}>Pay</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  card: {
    height: 200,
    paddingBottom: 20,
    paddingTop: 10,
    alignItems: 'center',
  },
  cardInput: {
    padding: 20,
  },
  cardidentifier: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  payBtnContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 10,
  },
  payBtn: {
    backgroundColor: Colors.Primary,
    height: 40,
    width: 200,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Checkout;
