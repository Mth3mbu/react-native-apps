import React, { useEffect, useCallback } from 'react';
import { isJwtExpired } from 'jwt-check-expiration';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import * as Colors from '../constants/Colors';
import { fetchToken } from '../helpers/db';
import { useDispatch } from 'react-redux';
import { LOGIN } from '../store/actions/auth';
import { clearSecurityTable } from '../helpers/db';

const SplashScreen = (props) => {
  const dispatch = useDispatch();

  const deleteEXistingToken = async () => {
    await clearSecurityTable();
  };

  const goToLogin = () => {
    props.navigation.navigate('Login');
  };

  const getAuthToken = useCallback(() => {
    const getToken = async () => {
      const dbResponse = await fetchToken();
      const data = dbResponse.rows._array[0];

      if (!data) {
        return goToLogin();
      }
      
      const isTokenExpired = isJwtExpired(data.token);

      if (isTokenExpired) {
        await deleteEXistingToken();
        return goToLogin();
      }

      dispatch({
        type: LOGIN,
        token: data.token,
      });

      props.navigation.navigate('Home');
    };
    getToken();
  }, []);

  useEffect(() => {
    getAuthToken();
  }, [getAuthToken]);

  return (
    <View style={styles.container}>
      <Text>Super Start</Text>
      <ActivityIndicator size='large' color={Colors.Primary} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SplashScreen;
