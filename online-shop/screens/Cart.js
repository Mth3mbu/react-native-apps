import React from 'react';
import { View, StyleSheet } from 'react-native';
import CartItems from '../components/ui/cart/CartListing';
import * as Colors from '../constants/Colors';

const Cart = (props) => {
  return (
    <View style={styles.container}>
      <CartItems navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 5,
  },
});

export const cartScreenOptions = (navData) => {
  return {
    headerStyle: {
      backgroundColor: Colors.Primary,
    },
    headerTintColor: Colors.White,
  };
};
export default Cart;
