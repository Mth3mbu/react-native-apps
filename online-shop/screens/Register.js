import React from 'react';
import { StyleSheet, TextInput, View, Button, ScrollView } from 'react-native';
import * as Colors from '../constants/Colors';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/ui/HeaderButtons';

const RegisterScreen = (props) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <TextInput
          style={styles.text}
          placeholder='First Name'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='Last Name'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='Email'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='Apartment/Flat'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='Street Name'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='City'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='Zip Code'
          returnKeyType='next'
        />
        <TextInput
          style={styles.text}
          placeholder='Password'
          secureTextEntry={true}
        />
        <View>
          <Button title='Register' color={Colors.Primary} />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 15,
  },
  text: {
    borderBottomColor: 'black',
    borderWidth: 1,
    padding: 10,
    marginVertical: 5,
  },
});

// RegisterScreen.navigationOptions = (navigationData) => {
//   return {
//     headerLeft: () => (
//       <HeaderButtons HeaderButtonComponent={HeaderButton}>
//         <Item
//           title='Menu'
//           iconName='ios-arrow-back-outline'
//           onPress={() => {
//             navigationData.navigation.pop();
//           }}
//         />
//       </HeaderButtons>
//     ),
//   };
// };
export default RegisterScreen;
