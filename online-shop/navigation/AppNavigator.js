import React from 'react';
import { Platform, View, SafeAreaView, Button } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerItemList,
} from '@react-navigation/drawer';
import { Ionicons } from '@expo/vector-icons';

import AboutScreen from '../screens/About';
import CheckoutScreen from '../screens/Checkout';
import CartScreen, { cartScreenOptions } from '../screens/Cart';
import LoginScreen, { loginScreenOptions } from '../screens/Login';
import ProfileScreen from '../screens/Profile';
import RegisterScreen from '../screens/Register';
import PasswordRecoveryScreen from '../screens/PasswordRecovery';
import HomeScreen, { screenOptions } from '../screens/Home';
import SplashScreen from '../screens/Splash';
import * as Colors from '../constants/Colors';

const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Colors.Primary,
  },
  headerTintColor: Colors.TextColor,
  headerTitleStyle: {
    fontWeight: 'bold',
  },
};

const StackNavigator = createStackNavigator();
const DrawerNavigator = createDrawerNavigator();

export const AppNavigator = (props) => {
  return (
    <StackNavigator.Navigator screenOptions={defaultNavOptions}>
      <StackNavigator.Screen
        name='Splash'
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <StackNavigator.Screen
        name='Login'
        component={LoginScreen}
        options={loginScreenOptions}
      />
      <StackNavigator.Screen name='Register' component={RegisterScreen} />
      <StackNavigator.Screen
        name='RecoverPassword'
        component={PasswordRecoveryScreen}
      />
      <StackNavigator.Screen
        name='Cart'
        component={CartScreen}
        options={cartScreenOptions}
      />
      <StackNavigator.Screen
        name='Checkout'
        component={CheckoutScreen}
        options={cartScreenOptions}
      />
      <StackNavigator.Screen
        name='Home'
        component={AppDrawerNavigator}
        options={{ headerShown: false }}
      />
    </StackNavigator.Navigator>
  );
};

export const AppDrawerNavigator = () => {
  return (
    <DrawerNavigator.Navigator
      drawerContent={(props) => {
        return (
          <View style={{ flex: 1, paddingTop: 20, backgroundColor: '#C0C0C0' }}>
            <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
              <DrawerItemList {...props} />
              <Button
                title='Logout'
                color={Colors.Primary}
                onPress={() => {}}
              />
            </SafeAreaView>
          </View>
        );
      }}
      screenOptions={screenOptions}
    >
      <DrawerNavigator.Screen
        name='Products'
        component={HomeScreen}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={Platform.OS === 'android' ? 'md-home' : 'ios-home-outline'}
              size={23}
              color={props.color}
            />
          ),
        }}
      />

      <DrawerNavigator.Screen
        name='Favourites'
        component={ProfileScreen}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={
                Platform.OS === 'android'
                  ? 'md-heart-outline'
                  : 'ios-heart-outline'
              }
              size={23}
              color={props.color}
            />
          ),
        }}
      />

      <DrawerNavigator.Screen
        name='About'
        component={AboutScreen}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={
                Platform.OS === 'android'
                  ? 'md-information-circle'
                  : 'ios-information-circle-outline'
              }
              size={23}
              color={props.color}
            />
          ),
        }}
      />
    </DrawerNavigator.Navigator>
  );
};

export default AppNavigator;
