import { ApiUrl } from '../constants/Enviroment';

export const pay = async (req, authToken) => {
  const results = await fetch(`${ApiUrl}/pay`, {
    headers: {
      'Authorization': `Bearer ${authToken}`,
      'Content-Type': 'application/json',
    },
    method: 'post',
    body: JSON.stringify(req),
  });

  return await results.json();
};
