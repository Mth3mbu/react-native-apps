import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('places.db');

export const init = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'CREATE TABLE IF NOT EXISTS Security (id INTEGER PRIMARY KEY NOT NULL,token TEXT NOT NULL)',
        [],
        () => {
          resolve();
        },
        (_, error) => {
          reject(error);
        }
      );
    });
  });

  return promise;
};

export const insertToken = (token) => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'INSERT INTO Security (token) VALUES (?)',
        [token],
        (_, results) => {
          resolve(results);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });

  return promise;
};

export const clearSecurityTable = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'DELETE FROM Security',
        [],
        (_, results) => {
          resolve(results);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });

  return promise;
};

export const fetchToken = () => {
  const promise = new Promise((resolve, reject) => {
    db.transaction((trans) => {
      trans.executeSql(
        'SELECT * FROM Security',
        [],
        (_, results) => {
          resolve(results);
        },
        (_, error) => {
          console.log(error);
          reject(error);
        }
      );
    });
  });

  return promise;
};
