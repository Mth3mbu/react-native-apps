import { STRIPE_PUBLISHABLE_KEY } from '../constants/Enviroment';

export const getCardToken = async (card) => {
  // Format the credit card data to a string of key-value pairs
  // divided by &
  const data = Object.keys(card)
    .map((key) => key + '=' + card[key])
    .join('&');

  const results = await fetch('https://api.stripe.com/v1/tokens', {
    headers: {
      // Use the correct MIME type for your server
      'Accept': 'application/json',
      // Use the correct Content Type to send data to Stripe
      'Content-Type': 'application/x-www-form-urlencoded',
      // Use the Stripe publishable key as Bearer
      'Authorization': `Bearer ${STRIPE_PUBLISHABLE_KEY}`,
    },
    // Use a proper HTTP method
    method: 'post',
    body: data,
  });

  const response = await results.json();
  return response.id;
};
