import jwt_decode from 'jwt-decode';
import { ApiUrl } from '../constants/Enviroment';

const createRequest = (items, token) => {
  var decoded = jwt_decode(token);

  const order = items.map((item) => {
    return {
      id: null,
      statusId: null,
      ProductId: item.id,
      userId: decoded.userId,
      orderNumber: null,
      quantity: item.totalItems,
      dateCreated: null,
    };
  });

  return {
    orders: order,
    userId: decoded.userId,
  };
};

export const createOrder = async (cartItems, authToken) => {
  const request = createRequest(cartItems, authToken);
  const results = await fetch(`${ApiUrl}/order`, {
    headers: {
      'Authorization': `Bearer ${authToken}`,
      'Content-Type': 'application/json',
    },
    method: 'post',
    body: JSON.stringify(request),
  });

  return true;
};
