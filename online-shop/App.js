import React from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import AuthReducer from './store/reducers/auth';
import ProductsReducer from './store/reducers/products';
import CartReducer from './store/reducers/cart';
import * as Colors from './constants/Colors';
import Main from './components/ui/Main';
import { init } from './helpers/db';

init()
  .then(() => {
    console.log('Initialized database');
  })
  .catch((err) => {
    console.log(err);
    console.log('Initializing database failed');
  });

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.White,
  },
  backgroundColor: Platform.OS === 'android' ? Colors.Primary : 'red',
};

const rootReducer = combineReducers({
  auth: AuthReducer,
  products: ProductsReducer,
  cart: CartReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
    <NavigationContainer theme={MyTheme}>
      <Provider store={store}>
        <Main />
      </Provider>
    </NavigationContainer>
  );
}
