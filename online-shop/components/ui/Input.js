import React from 'react';
import { StyleSheet, TextInput, View, Text } from 'react-native';
import * as Colors from '../../constants/Colors';

const Input = (props) => {
  return (
    <View>
      <TextInput
        {...props}
        style={{...styles.text, ...props.style}}
        placeholder={props.placeholder}
        value={props.value}
        onChangeText={props.onTextChanged}
        onBlur={props.lostFocus}
      />
      {!props.isValid && props.isTouched && props.value !== '' && (
        <Text style={styles.error}>{props.errorMessage}</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  error: {
    color: Colors.Danger,
    padding: 5,
  },
  text: {
    borderBottomColor: 'black',
    borderWidth: 1,
    padding: 10,
    marginVertical: 5,
  },
});
export default Input;
