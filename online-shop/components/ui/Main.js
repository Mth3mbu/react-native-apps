import React from 'react';
import { AppNavigator } from '../../navigation/AppNavigator';

export default function Main() {
  return <AppNavigator />;
}
