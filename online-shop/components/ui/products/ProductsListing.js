import React, { useEffect, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, FlatList, StyleSheet } from 'react-native';
import ProductItem from './ProductItem';
import * as ProductsActions from '../../../store/actions/products';
import Spinner from '../Spinner';

const ProductsListingScreen = (props) => {
  const jwtToken = useSelector((state) => state.auth.jwtToken);
  const productsList = useSelector((state) => state.products.products);
  const totalProducts = useSelector((state) => state.products.total);
  const [products, setProducts] = useState([]);

  const [loader, setLoader] = useState({
    isScreenLoading: false,
    isListLoading: false,
  });

  const [page, setPage] = useState(2);
  const totalItemsPerPage = 10;
  const dispatch = useDispatch();

  const getProductsTotal = useCallback(() => {
    const getTotalProducts = async () => {
      await dispatch(ProductsActions.getProductsTotal(jwtToken));
    };
    getTotalProducts();
  }, []);

  const fetProducts = useCallback(() => {
    const fetchProducts = async () => {
      setLoader({ ...loader, isScreenLoading: true });
      await dispatch(ProductsActions.fetchProducts(jwtToken, 0));
      setLoader({ ...loader, isScreenLoading: false });
    };
    fetchProducts();
  }, []);

  useEffect(() => {
    getProductsTotal();
    fetProducts();
  }, [fetProducts, getProductsTotal]);

  const productSelectHandler = (title, productId) => {
    props.navigation.navigate({
      routeName: 'Details',
      params: { productTitle: title, productId: productId },
    });
  };

  const renderItem = (itemData) => {
    return (
      <ProductItem
        navigation={props.navigation}
        image={itemData.item.image}
        name={itemData.item.name}
        price={itemData.item.price}
        productId={itemData.item.id}
        onSelect={productSelectHandler.bind(
          this,
          itemData.item.name,
          itemData.item.id
        )}
      />
    );
  };

  const loadMoreProducts = async () => {
    setLoader({ ...loader, isListLoading: true });
    setPage(page + 1);
    const offset = (page - 1) * totalItemsPerPage;
    if (products.length >= totalProducts) {
      return setLoader({ ...loader, isListLoading: false });
    }
    await dispatch(ProductsActions.fetchProducts(jwtToken, offset));
    setLoader({ ...loader, isListLoading: false });
  };

  useEffect(() => {
    setProducts(productsList);
  }, [productsList, products]);

  if (loader.isScreenLoading) {
    return <Spinner />;
  }

  return (
    <View style={styles.container}>
      <FlatList
        onEndReachedThreshold={0}
        onEndReached={loadMoreProducts}
        keyExtractor={(item) => item.id}
        data={products}
        numColumns={2}
        renderItem={renderItem}
        ListFooterComponent={loader.isListLoading && <Spinner />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default ProductsListingScreen;
