import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
  TouchableNativeFeedback,
} from 'react-native';
import { IconButton, Colors } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { AddToCart } from '../../../store/actions/cart';

const ProductItem = (props) => {
  const dispatch = useDispatch();

  let TouchableComponent = TouchableOpacity;
  if (Platform.OS === 'android' && Platform.Version >= 21) {
    TouchableComponent = TouchableNativeFeedback;
  }

  return (
    <View style={styles.container}>
      <TouchableComponent style={styles.wrapper} onPress={props.onSelect}>
        <View>
          <View style={styles.imgContainer}>
            <Image source={{ uri: props.image }} style={styles.image} />
          </View>
          <View style={styles.productInfo}>
            <Text>{props.name}</Text>
          </View>
          <View style={styles.actions}>
            <Text style={styles.price}>R{props.price}.00</Text>
            <IconButton
              icon='cart-plus'
              color={Colors.green700}
              size={20}
              onPress={() => dispatch(AddToCart(props.productId))}
            />
          </View>
        </View>
      </TouchableComponent>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    maxWidth: '48%',
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 10,
    margin: 4,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
    backgroundColor: '#FFF',
    overflow: 'hidden',
    elevation: 9,
    shadowOpacity: 0.32,
    overflow: 'hidden',
  },
  image: {
    height: 178,
    width: 178,
  },
  productInfo: {
    width: '100%',
    padding: 8,
    marginTop: 10,
  },
  price: {
    fontWeight: 'bold',
    fontSize: 15,
    paddingLeft: 10,
    paddingTop: 5,
  },
  imgContainer: {
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  wrapper: {
    flex: 1,
    overflow: 'hidden',
  },
});

export default ProductItem;
