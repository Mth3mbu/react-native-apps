import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Platform,
} from 'react-native';
import { useDispatch } from 'react-redux';
import Icon from './../Icon';
import * as Colors from '../../../constants/Colors';
import {
  AddToCart,
  RemoveItemFromCart,
  DeleteItemFromCart,
} from '../../../store/actions/cart';

import CartTotal from './CartTotal';

const CartItem = (props) => {
  const dispatch = useDispatch();

  const addMoreItemsHandler = (productId) => {
    dispatch(AddToCart(productId));
  };

  const removeItemHandler = (productId) => {
    dispatch(RemoveItemFromCart(productId));
  };

  const DeletItemHandler = (productId) => {
    dispatch(DeleteItemFromCart(productId));
  };

  return (
    <View>
      <View style={styles.container}>
        <View style={styles.body}>
          <View>
            <ImageBackground
              source={{ uri: props.image }}
              style={styles.image}
              resizeMode='cover'
            />
          </View>
          <View>
            <Text style={styles.font}>{props.name}</Text>
            <Text style={styles.price}>R {props.price}.00</Text>
            <Text>Items: {props.quantity}</Text>
          </View>
        </View>
        <View style={styles.actions}>
          <View style={styles.leftIcons}>
            <Icon
              icon={
                Platform.OS === 'android'
                  ? 'md-heart-outline'
                  : 'ios-heart-outline'
              }
            />
            <Icon
              onPress={DeletItemHandler.bind(this, props.productId)}
              icon={
                Platform.OS === 'android' ? 'md-trash' : 'ios-trash-outline'
              }
            >
              <Text>Remove</Text>
            </Icon>
          </View>
          <View style={styles.RightIcons}>
            <Icon
              onPress={removeItemHandler.bind(this, props.productId)}
              icon={
                Platform.OS === 'android'
                  ? 'md-remove-outline'
                  : 'ios-remove-outline'
              }
            />
            <View style={styles.quantity}>
              <Text>{props.quantity}</Text>
            </View>
            <Icon
              onPress={addMoreItemsHandler.bind(this, props.productId)}
              icon={Platform.OS === 'android' ? 'md-add' : 'ios-add'}
            />
          </View>
        </View>
      </View>
      {props.isLastItem && (
        <View style={styles.container}>
          <CartTotal total={props.total} navigation={props.navigation} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  actions: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#eeeeee',
    paddingTop: 10,
  },
  leftIcons: {
    width: '35%',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  quantity: {
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#e0e0e0',
    borderBottomWidth: 1,
  },
  RightIcons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  font: {
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    shadowOffset: { width: 0, height: 2 },
    marginBottom: 5,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    borderWidth: 1,
    borderColor: Colors.Primary,
    elevation: 8,
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 2,
  },
  body: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  price: {
    color: Colors.Primary,
  },
  image: {
    height: 60,
    width: 60,
    justifyContent: 'center',
    overflow: 'hidden',
  },
});

export default CartItem;
