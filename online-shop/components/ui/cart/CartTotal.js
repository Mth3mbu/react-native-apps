import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import * as Colors from '../../../constants/Colors';

const CartTotal = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Subtotal</Text>
        <Text style={styles.total}>R {props.total}.00</Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.title}>Total</Text>
        <Text style={styles.total}>R {props.total}.00</Text>
      </View>
      <TouchableOpacity onPress={() => props.navigation.navigate('Checkout')}>
        <View style={styles.checkout}>
          <Text style={styles.text}>Complete Order</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1 },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  body: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 1,
    borderTopWidth: 1,
    borderTopColor: '#e0e0e0',
  },
  title: { fontWeight: '700' },
  text: { color: 'white', fontWeight: '700' },
  checkout: {
    height: 40,
    flex: 1,
    backgroundColor: Colors.Primary,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    marginTop: 10,
  },
  total: {
    fontWeight: '500',
  },
});

export default CartTotal;
