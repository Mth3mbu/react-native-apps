import React, { useState, useEffect, useCallback } from 'react';
import { StyleSheet, View, FlatList, Text } from 'react-native';
import { useSelector } from 'react-redux';
import CartItem from './CartItem';

const CartListing = (props) => {
  const products = useSelector((state) => state.products.products);
  const items = useSelector((state) => state.cart.cartItems);
  const [cart, setCart] = useState({ items: [], total: 0 });

  let price = 0;
  const getCartItems = useCallback(() => {
    const selectedProducts = items.map((item) => {
      var productItem = products.find((product) => product.id === item.id);
      price = price + productItem.price * item.totalItems;
      return {
        name: productItem.name,
        image: productItem.image,
        price: productItem.price,
        quantity: item.totalItems,
        id: item.id,
      };
    });

    setCart({ ...cart, items: selectedProducts, total: price });
  }, [items]);

  useEffect(() => {
    getCartItems();
  }, [getCartItems]);

  return (
    <View style={styles.container}>
      {items.length > 0 ? (
        <FlatList
          numColumns={1}
          keyExtractor={(item) => item.id}
          data={cart.items}
          renderItem={(itemData) => (
            <CartItem
              navigation={props.navigation}
              productId={itemData.item.id}
              name={itemData.item.name}
              price={itemData.item.price}
              quantity={itemData.item.quantity}
              image={itemData.item.image}
              total={cart.total}
              isLastItem={
                itemData.item.id === cart.items[cart.items.length - 1].id
              }
            />
          )}
        />
      ) : (
        <View style={styles.empty}>
          <Text>Your cart is empty</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center' },
  empty: { flex: 1, justifyContent: 'center', alignItems: 'center' },
});

export default CartListing;
