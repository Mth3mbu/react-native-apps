import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import * as Colors from '../../constants/Colors';

const Icon = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View style={styles.icon}>
        <Ionicons name={props.icon} size={23} color={Colors.Primary} />
        {props.children}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  icon: { flexDirection: 'row', alignItems: 'center' },
});

export default Icon;
