import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import * as Colors from '../../constants/Colors';

const Spinner = (props) => {
  return (
    <View style={styles.spinner}>
      <ActivityIndicator color={Colors.Primary} size='large' />
    </View>
  );
};

const styles = StyleSheet.create({
  spinner: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default Spinner;
